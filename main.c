/*
 * File:   main.c
 * Author: luis d�az rojas
 *
 * Created on 2 de abril de 2022, 1:14 a.m.
 */


#include <xc.h>

#pragma config FOSC = HS
#pragma config WDTE = OFF
#pragma config PWRTE = OFF
#pragma config MCLRE = ON
#pragma config CP = OFF
#pragma config CPD = OFF
#pragma config BOREN = OFF
#pragma config IESO = OFF
#pragma config FCMEN = OFF
#pragma config LVP = OFF
#pragma config DEBUG = OFF
# define _XTAL_FREQ 20000000
void main(void) {
     TRISC = 0x00; // Pone e l RB0 como s a l i d a .
    PORTC = 0x00;
    while ( 1 )
{
PORTC=0X0A; // Encender e l LED.
__delay_ms (100) ; // Retardo de un segundo .
PORTC=0X08; 
__delay_ms (50) ; 
PORTC=0X0A; 
__delay_ms (100) ; 
PORTC=0X02; 
__delay_ms (50) ; 
PORTC=0X05; 
__delay_ms (100) ; 
PORTC=0X08; 
__delay_ms (50) ;
PORTC=0X05; 
__delay_ms (100) ; 
PORTC=0X00; 
__delay_ms (50) ; 
    }
    return;
}
